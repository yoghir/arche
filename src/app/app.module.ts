import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';

import {Amplify, Interactions } from 'aws-amplify';
import { HomeComponent } from './home/home.component';
import { FlexModule } from '@angular/flex-layout';
Amplify.configure({
  Auth: {
    identityPoolId: 'us-east-1:14eb3cb6-8b1e-4640-a0d2-c53ccac6c424',
    region: 'us-east-1',
    userPoolId: "us-east-1_0Dkzz1XuM",
    userPoolWebClientId: "76sp5r3lcplaaoqaab6opman1r"
  },
  Interactions: {
    bots: {
      "Arche_dev": {
        "name": "Arche_dev",
        "alias": "$LATEST",
        "region": "us-east-1",
      },
    }
  }
});

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    FlexModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
