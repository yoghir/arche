import { Component, OnInit, Input, inject, Inject } from '@angular/core';
import { Interactions } from 'aws-amplify';
import { Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  constructor(private router: Router) { }
  ngOnInit(): void {
  }

}