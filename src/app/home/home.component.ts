import { Component } from '@angular/core';
import { Interactions } from 'aws-amplify';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  dydiv = new Array();
  title = 'Lex-App';
  conversation: string = '';
  userinput =new Array();
  botresponse=new Array();
  check =new Array();
  message!: string;
  u1:boolean ;
  isBotOpened: boolean;
  output: string = '';
innerHTML: any;
  constructor() { 
    this.isBotOpened = false;
    this.u1 = true;
  }
  ngOnInit(): void {
  }

  async startChat() {
    // Provide a bot name and user input
    this.dydiv.push(this.dydiv.length);
    
    this.check.push("<BR>" +"\nYou::" + this.message);
    this.conversation = this.conversation + "<BR>" +"\nYou::" + this.message;
    var response = await Interactions.send("Arche_dev", this.message.toString());
    //Log chatbot response
    console.log(response);
    this.userinput.push( "<BR>" +"\nYou::" + this.message);
    this.message = '';
    var url = "https://www.fca.org.uk/firms/regdata/data-reference-guides";
    var url2 = "https://www.fca.org.uk/multimedia/fca-explains-regdata-logging";
    var url3 = "https://www.fca.org.uk/multimedia/fca-explains-regdata-changing-your-password";
    var url4 = "https://www.fca.org.uk/multimedia/fca-explains-regdata-changing-user-details";
    var url5 = "https://www.fca.org.uk/multimedia/fca-explains-regdata-types-reporting";
    var urlout1 = "";
    var urlout2 = "";
    var urlout3 = "";
    var urlout4 = "";
    var urlout5 = "";
    
    
    var optiontxt = "Are you looking for below assistance?";
    
    if(response && response['message']){
      this.conversation = this.conversation + "<BR>" + "\nBot::" +  response['message']
      
      
      this.botresponse.push( "<BR>" + "\nBot::" +  response['message']);
      this.check.push("<BR>" + "\nBot::" + response['message']);
      
     /* if(response['message'].includes(url) && this.u1){
      this.u1 = false;  
      urlout1 = "<a href=https://www.fca.org.uk/firms/regdata/data-reference-guides target=\"_blank\">Click here</a>";
      this.conversation =  this.conversation.replaceAll(url,urlout1); 
      //this.conversation =  this.conversation.replaceAll("---","<BR>"); 
      //this.conversation =  this.conversation;
     // this.output =  this.conversation;

    } 

  
    if (response['message'].includes(url2)){
      urlout2 = "<a href=https://www.fca.org.uk/multimedia/fca-explains-regdata-logging target=\"_blank\">Click here</a>"
      this.conversation =  this.conversation.replace(url2,urlout2); 
      //this.conversation =  this.conversation.replaceAll("---","<BR>"); 
      
    }
    if (response['message'].includes(url3)){
      urlout3 = "<a href= https://www.fca.org.uk/multimedia/fca-explains-regdata-changing-your-password target=\"_blank\">Click here</a>"
      this.conversation =  this.conversation.replace(url3,urlout3); 
      //this.conversation =  this.conversation.replaceAll("---","<BR>"); 
      
    }
    if (response['message'].includes(url4)){
      urlout4 = "<a href=https://www.fca.org.uk/multimedia/fca-explains-regdata-changing-user-details target=\"_blank\">Click here</a>"
      this.conversation =  this.conversation.replace(url4,urlout4); 
      //this.conversation =  this.conversation.replaceAll("---","<BR>"); 
      
    }
    if (response['message'].includes(url5)){
      urlout5 = "<a href=https://www.fca.org.uk/multimedia/fca-explains-regdata-types-reporting target=\"_blank\">Click here</a>"
      this.conversation =  this.conversation.replace(url5,urlout5); 
      //this.conversation =  this.conversation.replaceAll("---","<BR>"); 
      
    }  */

    if (response['message'].includes(optiontxt)){
      this.conversation =  this.conversation.replaceAll("---","<BR>"); 
      
      
    }
  }
    
    if(response && !response['message']){
      this.conversation = this.conversation + "\nBot::" + "Your SR Request is complete."

    }
  }
  getCurrentTime(): string {
    const date = new Date();
    const hours = date.getHours();
    const minutes = date.getMinutes();
    const seconds = date.getSeconds();
    return `${hours}:${minutes}:${seconds}`;
  }
  getChatBox(){
    this.isBotOpened = !this.isBotOpened;
  }
}
